class Student {
    onstructor({ name, email, grades }) {
        this.name = name;
        this.email = email;
        this.grades = (grades.every(grade => (grade >= 0 && grade <= 100)) && grades.length === 4) ? grades : undefined;
    }

    login() {
        console.log(`${this.email} is logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} is logged out`);
        return this;
    }
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
        return this;
    }

    computeAve() {
        this.gradeAve = this.grades.reduce((a, b) => a + b) / this.grades.length;
        return this;
    }
    willPass() {
        this.passed = this.gradeAve >= 85;
        return this;
    }
    willPassWithHonors() {

        if (this.passed) {
            this.passedWithHonors = this.gradeAve >= 90;
        } else {
            this.passedWithHonors = undefined;
        }
        return this;
    }

}

class Section {
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorsPercentage = undefined;
        this.honorStudents = undefined;
    }

    addStudent({ name, email, grades }) {
        this.students.push(new Student({ name, email, grades }));
        return this;
    }

    countHonorStudents() {

        let count = 0;
        this.students.forEach(student => {
            if (student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
                count++;
            }
        })

        this.honorStudents = count;
        return this;
    }


    computeHonorsPercentage() {
        this.honorsPercentage = (this.honorStudents / 4) * 100;
        // console.log(this);
        return this;
    }
}


class Grade {
    constructor(gradeLevel) {
        this.gradeLevel = gradeLevel;
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }


    addSection(name) {
        this.sections.push(new Section(name));
        return this;
    }


    countStudents() {
        for (let section of this.sections) {
            for (let student of section.students) {
                this.totalStudents++;
            };
        };
        return this;
    }


    countHonorStudents() {
        for (let section of this.sections) {
            for (let student of section.students) {
                student.computeAve().willPass().willPassWithHonors();
                student.passedWithHonors ? this.totalHonorStudents++ : this.totalHonorStudents
            };
        };
        return this;
    }


    computeBatchAve() {
        countStudents();
        let totalGrades = 0;

        for (let section of this.sections) {
            for (let student of section.students) {
                totalGrades += student.computeAve().gradeAve;
            };
        };
        this.batchAveGrade = totalGrades / this.totalStudents;
        return this;
    }


    getBatchMinGrade() {
        this.batchMinGrade = 100;
        for (let section of this.sections) {
            for (let student of section.students) {
                for (let grade of student.grades)
                    if (grade < this.batchMinGrade) this.batchMinGrade = grade;
            };
        };
        return this;
    }



    getBatchMaxGrade() {
        this.batchMaxGrade = 0;
        for (let section of this.sections) {
            for (let student of section.students) {
                for (let grade of student.grades)
                    if (grade > this.batchMaxGrade) this.batchMaxGrade = grade;
            };
        };
        return this;
    }

}

// 3. write the statement that i will add a students to sections1A
let NameOfsection = "section1A"
let grade1 = new Grade(1);
grade1.addSection(NameOfsection)

grade1.sections.find(section => section.name === NameOfsection).addStudent({ email: "john@mail", name: "john", grades: [89, 84, 77, 88] })
grade1.sections.find(section => section.name === NameOfsection).addStudent({ email: "jooe@mail", name: "joe", grades: [72, 82, 79, 85] })
grade1.sections.find(section => section.name === NameOfsection).addStudent({ email: "jone@mail.com", name: "jane", grades: [87, 89, 91, 83] })
grade1.sections.find(section => section.name === NameOfsection).addStudent({ email: "jessie", name: "jessi", grades: [91, 89, 92, 93] })


